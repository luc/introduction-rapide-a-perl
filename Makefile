PANDOC=pandoc --number-sections -f markdown+grid_tables+superscript --template=intro_a_perl.tex
PANDOCHTML=pandoc --number-sections -f markdown+grid_tables+superscript --template=../asrall.html -V cours="Introduction rapide à Perl" --toc --toc-depth=1
all: pdf html

pdf:
	$(PANDOC) intro_a_perl.md -o intro_a_perl.pdf \
		-V title-meta="Introduction rapide à Perl" \
		-V author-meta="Luc Didry & Julien Vaubourg"
html: pdf
	sed -e "s@\\\\textsc{\([^}]\+\)}@\1@g" intro_a_perl.md > intro_a_perl_html.md
	$(PANDOCHTML) intro_a_perl_html.md -o ../html/introduction_a_perl/index.html \
		-V author-meta="Luc Didry & Julien Vaubourg" \
		-V title="Introduction rapide à Perl" \
		-V pdf="intro_a_perl.pdf" \
		-V out="index.html"
	rm intro_a_perl_html.md
	sed -i -e 's@ *<li> <a href="index.html">Introduction rapide à Perl</a> /</li>@@' ../html/introduction_a_perl/index.html
	cp intro_a_perl.pdf ../html/introduction_a_perl/
