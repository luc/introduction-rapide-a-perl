# Introduction rapide à Perl

Ceci est un petit cours rapide de Perl créé par [Julien Vaubourg](https://julien.vaubourg.com/) et [Luc Didry](https://luc.frama.io) pour la [licence professionnelle ASRALL](http://asrall.fr) (Administration de Systèmes, Réseaux et Applications à base de Logiciels Libres).

Ce cours a été créé en 2009 et révisé en 2016.

## Téléchargement

Vous pouvez récupérer le PDF compilé sur <https://luc.frama.io/cours-asrall/introduction_a_perl/intro_a_perl.pdf>.

## Compilation

```
sudo apt-get install texlive-full pandoc make
git clone https://framagit.org/luc/introduction-rapide-a-perl.git
cd introduction-rapide-a-perl
make
```

## Licence

Ce cours est soumis à la licence CC-BY-SA 2.0. Voir le [fichier LICENSE](LICENSE) pour plus de détails.
